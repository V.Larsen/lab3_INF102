package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    private int numberGuess;

	@Override
    public int findNumber(RandomNumber number) {
        
        int lowerBound = number.getLowerbound();
		int upperBound = number.getUpperbound();

        while (lowerBound <= upperBound) {
            int numberGuess = (lowerBound + upperBound) / 2;

            if (number.guess(numberGuess) == 0) {
                return numberGuess;
            }
            else if (number.guess(numberGuess) == -1){
                lowerBound = numberGuess + 1;
            }
            else if (number.guess(numberGuess) == 1){
                upperBound = numberGuess - 1;
            }
        }
        return numberGuess;
    }

}
